# Homepls ver. 1.0

The project helps the user find different alternatives (regarding time and money) to travel home in a compact and easy manner to understand, while also giving alternatives and options to act on those alternatives.

[homepls project source](https://bitbucket.org/aass16team07/homepls)


## Contact the Developers
- Jessica Concepcion
- Peter Szabo

## Main Component Screenshots
### Mainscreen                &               Buddyscreen

![Mainscreen](https://imageshack.com/i/plvjzqeSj) 
![Buddy](https://imageshack.com/i/poDSkgqPj)
 
 ### Settings & public transport
 
![Settings](https://imageshack.com/i/pnnCoFBhj)
![publicTransport](https://imageshack.com/i/pmEp91NFj)

 ### Walking & route finding
 
![Walk](https://imageshack.com/i/povsHcXLj)
![routing](https://imageshack.com/i/pmCm8iqWj)
 
### External libraries/frameworks/etc.

Projects being used in this app e.g. :

- Android API 23 Platform
- Java 1.8
- animated-vector-drawable-23.4.0
- appcompat-v7-23.4.0
- hamcrest-core-1.3
- junit-4.12
- percent-23.3.0
- play-services-base-8.4.0
- play-services-basement-8.4.0
- play-services-location-8.4.0
- play-services-maps-8.4.0
- support-annotations-23.4.0
- support-v4-23.4.0
- support-vector-drawable-23.4.0




## Getting Started

Install of app:
1. Download android studio (https://developer.android.com/studio/index.html) 
2. Install Android studios
3. Download the project (https://bitbucket.org/aass16team07/homepls) and store it in your chosen location.
4. Open the project from android studio (It should be marked as an android studio file) and wait for it to load.
5. Run the project in android studios on one of your chosen emulators

## Usecase
1. Fred has been out on the town partying.
2. Fred opens Homepls
3. Fred looks at the route he can take home with the bus, but decides against it since its going to take too long of a time.
4. He presses the friend tab, but decides he cant bother his friends this late
5. He presses the taxi tab and the app calls a taxi

## FAQ
    Q: What is the purpose of Homepls
    A: Giving people an overview and alternatives on how to to get home
    
    Q: What is the underlying technology for Homepls?
    A: Google maps api, and it integrates whatsapp for friend calling

## Testing

To work on unit tests, switch the Test Artifact in the Build Variants view.

## Licence
No licence defined